package com.devcamp.voucher.service;

import java.util.ArrayList;
import com.devcamp.voucher.Model.CVoucher;
import com.devcamp.voucher.respository.CVoucherResponsitory;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class CVoucherService {
    @Autowired
	CVoucherResponsitory pVoucherRepository;
    public ArrayList<CVoucher> getVoucherList() {
        ArrayList<CVoucher> listCVoucher = new ArrayList<>();
        pVoucherRepository.findAll().forEach(listCVoucher::add);
        return listCVoucher;
    }
}

