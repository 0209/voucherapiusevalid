package com.devcamp.voucher.respository;

import com.devcamp.voucher.Model.CVoucher;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CVoucherResponsitory extends JpaRepository<CVoucher,Long>{
    
}
