package com.devcamp.voucher.controller;

import com.devcamp.voucher.respository.CVoucherResponsitory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.voucher.Model.CVoucher;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CVoucherControllerDelete {
    
    @Autowired
    CVoucherResponsitory pVoucherResponsitory;

    @DeleteMapping("/voucher/{id}")
    public ResponseEntity<CVoucher> deleteVoucherById(@PathVariable("id") long id){
        try {
            pVoucherResponsitory.deleteById(id);
            return new ResponseEntity<CVoucher>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
