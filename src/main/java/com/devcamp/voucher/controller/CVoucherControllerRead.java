package com.devcamp.voucher.controller;

import com.devcamp.voucher.respository.CVoucherResponsitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.ArrayList;
import com.devcamp.voucher.Model.CVoucher;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CVoucherControllerRead {
    
    @Autowired
    CVoucherResponsitory pVoucherResponsitory;

    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getAllVoucher() {
        try {
            List<CVoucher> allVouchers = new ArrayList<CVoucher>();
            pVoucherResponsitory.findAll().forEach(allVouchers::add);
            return new ResponseEntity<>(allVouchers,HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
