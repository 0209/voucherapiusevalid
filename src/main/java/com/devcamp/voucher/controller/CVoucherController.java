package com.devcamp.voucher.controller;

import com.devcamp.voucher.respository.CVoucherResponsitory;
import com.devcamp.voucher.service.CVoucherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.List;

import com.devcamp.voucher.Model.CVoucher;
@RestController
@RequestMapping("/")
@CrossOrigin
public class CVoucherController {
    
    @Autowired
    CVoucherService pVoucherService;

    @Autowired
    CVoucherResponsitory pVoucherResponsitory;

    @GetMapping("/servicevouchers")
    public ResponseEntity<List<CVoucher>> getAllVouchers(){
        try {
            return new ResponseEntity<>(pVoucherService.getVoucherList(),HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        } 
    }
}
