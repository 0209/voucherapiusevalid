package com.devcamp.voucher.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Optional;
import com.devcamp.voucher.Model.CVoucher;
import com.devcamp.voucher.respository.CVoucherResponsitory;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CVoucherControllerReadId {
    
    @Autowired
    CVoucherResponsitory pVoucherResponsitory;

    @GetMapping("/vouchers/{id}")
    public ResponseEntity<CVoucher> getVoucherById(@PathVariable("id") long id){
        Optional<CVoucher> voucherData = pVoucherResponsitory.findById(id);
        if (voucherData.isPresent()) {
            return new ResponseEntity<>(voucherData.get(),HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
