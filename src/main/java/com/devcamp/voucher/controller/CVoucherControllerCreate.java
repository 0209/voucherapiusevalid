package com.devcamp.voucher.controller;

import java.util.Date;
import java.util.Optional;

import javax.validation.Valid;

import com.devcamp.voucher.Model.CVoucher;
import com.devcamp.voucher.respository.CVoucherResponsitory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CVoucherControllerCreate {

    @Autowired
    CVoucherResponsitory pVoucherResponsitory;

    @PostMapping("/vouchers")
    public ResponseEntity<Object> createVoucher(@Valid @RequestBody CVoucher pVouchers) {
        try {
            CVoucher _voucher = pVoucherResponsitory.save(pVouchers);
            return new ResponseEntity<>(_voucher,HttpStatus.CREATED);
        } catch (Exception e){
            System.out.println(e);
            return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);
        }
    }
}
