package com.devcamp.voucher.controller;

import java.util.Date;
import java.util.Optional;

import com.devcamp.voucher.respository.CVoucherResponsitory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.voucher.Model.CVoucher;
@RestController
@RequestMapping("/")
@CrossOrigin
public class CVoucherControllerUpdate {
    
    @Autowired
    CVoucherResponsitory pVoucherResponsitory;

    @PutMapping("/vouchers/{id}")
    public ResponseEntity<Object> updateVoucherById(@PathVariable("id") long id,@RequestBody CVoucher pVouchers){
        Optional<CVoucher> voucherData = pVoucherResponsitory.findById(id);
        if(voucherData.isPresent()){
            CVoucher voucher = voucherData.get();
            voucher.setMaVoucher(pVouchers.getMaVoucher());
            voucher.setGhiChu(pVouchers.getGhiChu());
            voucher.setNgayCapNhat(new Date());
            try {
                return new ResponseEntity<>(pVoucherResponsitory.save(voucher),HttpStatus.OK);
            } catch(Exception e){
                return ResponseEntity.unprocessableEntity().body("Failed to Update specified Voucher:"+e.getCause().getCause().getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Failed to get specified Voucher: "+id + "  for update.");
        }
    }
}
