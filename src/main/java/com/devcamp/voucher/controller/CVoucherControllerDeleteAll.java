package com.devcamp.voucher.controller;

import com.devcamp.voucher.Model.CVoucher;
import com.devcamp.voucher.respository.CVoucherResponsitory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CVoucherControllerDeleteAll {
    
    @Autowired
    CVoucherResponsitory pVoucherResponsitory;

    @DeleteMapping("/vouchers")
    public ResponseEntity<CVoucher> deleteAllCVoucher(){
        try {
            pVoucherResponsitory.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch(Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
